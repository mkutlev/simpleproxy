package com.simpleproxy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MainActivity extends Activity {

    private EditText mUrlEditText;

    private Button mGoButton;

    private ListView mInfoList;

    private WebView mWebView;

    private ArrayAdapter<String> mListAdapter;

    private List<String> mRedirects = new ArrayList<String>();

    private List<List<String>> mRedirectHeaders = new ArrayList<List<String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpListView();

        mUrlEditText = (EditText)findViewById(R.id.urlEditText);

        mWebView = (WebView)findViewById(R.id.webView);
        mWebView.setWebViewClient(new MyWebViewClient());

        mGoButton = (Button)findViewById(R.id.goButton);
        mGoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                connect(mUrlEditText.getText().toString());
            }
        });

    }

    private void connect(String url) {
        ConnectionTask task = new ConnectionTask();
        task.execute(url);
    }

    private void loadUrlInWebView(String url) {
        mWebView.loadUrl(url);
    }

    private void setUpListView() {
        mInfoList = (ListView)findViewById(R.id.list);
        mListAdapter = new ArrayAdapter<String>(this, R.layout.list_item);

        mInfoList.setAdapter(mListAdapter);

        mInfoList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showHeaders(position);
            }
        });
    }

    private void setRedirects(List<String> redirectsShortInfo, List<String> redirects) {
        mRedirects = redirects;
        mRedirects = redirectsShortInfo;
        mListAdapter.clear();

        for (String redirect : mRedirects) {
            mListAdapter.add(redirect);
        }
    }

    private class ConnectionTask extends AsyncTask<String, Void, String> {

        private List<String> mRedirects;

        private List<String> mRedirectsShortInfo;

        @Override
        protected String doInBackground(String... params) {
            mRedirects = new ArrayList<String>();
            mRedirectsShortInfo = new ArrayList<String>();
            HttpURLConnection conn = null;

            StringBuilder redirect = new StringBuilder();
            int responseCode = 0;
            String locationUrl = params[0];

            while (true) {
                redirect.delete(0, redirect.length());
                try {
                    conn = (HttpURLConnection)(new URL(locationUrl).openConnection());
                    conn.setInstanceFollowRedirects(false);
                    conn.connect();

                    Map<String, List<String>> headers = conn.getHeaderFields();
                    Iterator<Entry<String, List<String>>> iterator = headers.entrySet().iterator();

                    responseCode = conn.getResponseCode();
                    redirect.append("ResponseCode: ").append(responseCode).append("\nRedirect: ")
                            .append(conn.getHeaderField("Location"));
                    mRedirectsShortInfo.add(redirect.toString());

                    List<String> headerPairs = new ArrayList<String>();

                    while (iterator.hasNext()) {
                        Entry<String, List<String>> pairs = iterator.next();
                        headerPairs.add(pairs.getKey() + " = " + pairs.getValue());
                    }

                    mRedirectHeaders.add(headerPairs);

                    mRedirects.add(redirect.toString());

                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        // Returns the last loaded url
                        return locationUrl;
                    } else if (conn.getHeaderField("Location") == null) {
                        // We are not redirected and response code is not OK

                        return null;
                    }

                    locationUrl = conn.getHeaderField("Location");

                    Log.e("TAG", "ResponseCode: " + responseCode + "; Location: " + locationUrl);

                    Log.e("TAG", "HeaderFields" + conn.getHeaderFields());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }

        }

        @Override
        protected void onPostExecute(String url) {
            if (url != null) {
                loadUrlInWebView(url);
            } else {
                loadUrlInWebView("about:blank");
                Toast.makeText(MainActivity.this, "No Response code 200!", Toast.LENGTH_SHORT)
                        .show();
            }

            setRedirects(mRedirectsShortInfo, mRedirects);
        }

    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

    }

    private void showHeaders(int redirectNum) {
        AlertDialog.Builder builder = new Builder(this);
        builder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();

        ListView listView = (ListView)inflater.inflate(R.layout.dialog_layout, null);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item);

        for (String header : mRedirectHeaders.get(redirectNum)) {
            adapter.add(header);
        }

        listView.setAdapter(adapter);

        builder.setView(listView);
        builder.show();
    }

}
